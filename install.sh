#!/bin/bash
# Pleroma, a simple script to get a nicely configured BSPWM desktop up and running on Void Linux
# Version 1.3.0, updated 2020-05-19
clear

bypass() {
  sudo -v
  while true;
  do
    sudo -n true
    sleep 45
    kill -0 "$$" || exit
  done 2>/dev/null &
}

echo "Welcome to Pleroma, a simple script to get a nicely configured BSPWM desktop up and running on Void Linux."
read -p "Would you like to continue (y/n)? " installChoice

case "$installChoice" in
  # User wants to continue installation
  y|Y|yes|Yes|YES )

    # Make sure user is running as superuser
    echo "Enter your sudo password to continue."
    bypass

    read -p "Would you like to perform a system upgrade before continuing (y/n)? " upgradeChoice
    case "$upgradeChoice" in
      y/Y/yes/Yes/YES )
        echo "Upgrading system..."
        sudo xbps-install -Syu
      ;;
      n/N/no/No/NO )
        echo "Skipping system upgrade."
      ;;
    esac

    echo "Installing multilib and non-free repos..."
      sudo xbps-install -y void-repo-multilib void-repo-multilib-nonfree void-repo-nonfree

    echo "Preparing to install packages..."
      echo "Installing build essentials and kernel headers..."
        sudo xbps-install -y linux-headers linux-tools gcc ctags make cmake rsync
      echo "Installing graphics drivers..."
        sudo xbps-install -y vkd3d vkd3d-32bit vulkan-loader vulkan-loader-32bit
      # Check for an NVIDIA graphics card and install proprietary drivers if found
      case $(lspci -k | grep -A 2 -E "(VGA|3D)") in
        *NVIDIA*)
          musl=$(ldd /bin/ls | grep 'musl' | head -1 | cut -d ' ' -f1)
          if [ -z $musl ]; then
              # NVIDIA detect and user is on a Glibc system
              echo "Detected NVIDIA graphics card. Installing proprietary drivers..."
              sudo xbps-install -y linux-firmware-nvidia nvidia nvidia-libs nvidia-libs-32bit libGL-32bit
          else
              # If user is on a system with Musl, driver installation is skipped.
              echo "NVIDIA proprietary drivers cannot be installed on a Musl system...skipping."
              exit 1
          fi
        ;;
      esac
      echo "Installing fonts..."
        sudo xbps-install -y fontconfig font-iosevka ttf-material-icons nerd-fonts
      echo "Installing audio packages..."
        sudo xbps-install -y alsa-utils alsa-plugins-pulseaudio ffmpeg lame pamixer pulseaudio x264
      echo "Installing desktop environment..."
        sudo xbps-install -y betterlockscreen bottom bspwm cava compton dunst picom polybar redshift rofi sxhkd rxvt-unicode
      echo "Installing utilities and system tools..."
        sudo xbps-install -y android-tools bind-utils curl dbus-elogind elogind feh gnutls gnutls-32bit gvfs gvfs-mtp gzip NetworkManager ntp procps-ng udisks2 unrar unzip vpm vsv wget whois wireguard youtube-dl zip
      echo "Installing additional applications..."
        # Edit the following list of additional applications or replace them with your own preferences
        # Code editor
        musl=$(ldd /bin/ls | grep 'musl' | head -1 | cut -d ' ' -f1)
        if [ -z $musl ]; then
          # User is on a Glibc system
          echo "Installing Atom..."
          sudo xbps-install -y atom
        else
          # User is on a Musl system
          echo "Cannot install Atom on a Musl system...skipping."
          exit 1
        fi
        # Audio editor
        echo "Installing Audacity..."
        sudo xbps-install -y audacity
        # LED control for Corsair keyboards and mice
        echo "Installing ckb-next..."
        sudo xbps-install -y ckb-next
        # PDF reader
        echo "Installing XPDF..."
        sudo xbps-install -y xpdf
        # Web browser
        echo "Installing Firefox..."
        sudo xbps-install -y firefox
        # Screenshot utility
        echo "Installing Flameshot..."
        sudo xbps-install -y flameshot
        # Image editor
        echo "Installing GIMP..."
        sudo xbps-install -y gimp
        # Office suite
        echo "Installing LibreOffice..."
        sudo xbps-install -y libreoffice-calc libreoffice-gnome libreoffice-i18n-en-US libreoffice-writer
        # Audio and video player
        echo "Installing MPV..."
        sudo xbps-install -y mpv
        # Text editor
        echo "Installing nano..."
        sudo xbps-install -y nano
        # Audio recording and streaming
        echo "Installing OBS Studio..."
        sudo xbps-install -y obs
        # File manager...atool is installed for the ranger_archives plugin to work properly
        echo "Installing Ranger..."
        sudo xbps-install -y ranger atool
        # Voice conferencing
        echo "Installing Skype..."
        sudo xbps-install -y skype
        # Gaming
        echo "Installing Lutris..."
        sudo xbps-install -y lutris
        echo "Installing Steam..."
        sudo xbps-install -y steam
        echo "Installing Wine..."
        sudo xbps-install -y wine wine-32bit
        # Install ZSH and Oh My ZSH
        echo "Installing ZSH..."
        sudo xbps-install -y zsh
        sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
        # Install Flatpak support and add Flathub repo
        echo "Installing Flatpak support..."
        sudo xbps-install -y flatpak
        flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        # Install Spotify and theme support with Spicetify..."
        echo "Installing Spotify..."
        sudo xbps-install -y spotify
        sudo xbps-install -y spotify-adblock-linux

    echo "Configuring system..."
      echo "Setting up services..."
        sudo sed -i "s/--noclear/--noclear\ --skip-login\ --login-options=$USER/g" /etc/sv/agetty-tty1/conf
	sudo rm -f /var/service/agetty-tty{3,4,5,6}
        sudo ln -s /etc/sv/ckb-next-daemon /var/service/
	sudo ln -s /etc/sv/dbus /var/service/
        sudo ln -s /etc/sv/elogind /var/service/
        sudo ln -s /etc/sv/NetworkManager /var/service/
	sudo ln -s /etc/sv/polkitd /var/service/
	sudo ln -s /etc/sv/uuidd /var/service/
      echo "Updating nameservers..."
	sudo resolvconf -u
      echo "Configuring font settings..."
	sudo ln -s /usr/share/fontconfig/conf.avail/10-hinting-slight.conf /etc/fonts/conf.d/
	sudo ln -s /usr/share/fontconfig/conf.avail/10-sub-pixel-rgb.conf /etc/fonts/conf.d/
      	sudo ln -s /usr/share/fontconfig/conf.avail/11-lcdfilter-default.conf /etc/fonts/conf.d/
	sudo ln -s /usr/share/fontconfig/conf.avail/50-user.conf /etc/fonts/conf.d/
	sudo ln -s /usr/share/fontconfig/conf.avail/70-no-bitmaps.conf /etc/fonts/conf.d/
      echo "Importing dotfiles..."
        # The following assumes your are running the script as stated in the README: bash pleroma/install.sh. If you are running the script from inside the pleroma directory (cd pleroma; bash install.sh), change "cp -r pleroma/home/* $HOME/" to "cp -r home/* $HOME/"
        shopt -s dotglob
        cp -r pleroma/home/* $HOME/
        chmod +x $HOME/.config/ranger/scope.sh
        # Install Spicetify for Spotify theming support
        read -p "Would you like add Spotify theming support via Spicetify (y/n)? " spicetifyChoice
        case "$spicetifyChoice" in
          y/Y/yes/Yes/YES )
            sudo chmod a+wr /usr/share/spotify
            sudo chmod a+wr /usr/share/spotify/spotify-client
            sudo chmod a+wr /usr/share/spotify/spotify-client/Apps -R
            curl -fsSL https://raw.githubusercontent.com/khanhas/spicetify-cli/master/install.sh | sh
          ;;
          n/N/no/No/NO )
            rm -rf $HOME/.config/spicetify
            echo "Skipping Spicetify installation."
          ;;
        esac
        # END SPICETIFY INSTALL
      echo "Changing default shell to ZSH..."
        chsh -s /usr/bin/zsh $USER

    echo "All done! Please reboot for all changes to take effect."
  ;;

  # User does not want to continue installation.
  n|N|no|No|NO )
    echo "Thanks for trying, Pleroma. Goodbye!";;
esac
